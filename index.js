const app = require('express')();
const server = require('http').Server(app);
const cors = require('cors');
const bodyParser = require('body-parser');
//let io = require('socket.io').listen(3001, { origins: 'http://localhost:3002' });
const io = require('socket.io')(server, { path: '/alarmApi/socket' });

const routes = require('./routes');
const tm = require('./controllers/tokenManager');
const sm = require('./websocket/streamManager');
const alarm = require('./logic/alarmAnalyzer');

const corsOptions = {
  origin: 'http://localhost:3002'
}
app.use(cors(corsOptions));
app.use(bodyParser.json());

app.use('/alarmApi', routes);

if (!module.parent) {
  const port = process.env.PORT || 3000;
  server.listen(port);
  console.log('Express started on port ' + port);
}

sm.start(io);
tm.start();
alarm.start();
