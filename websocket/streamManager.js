
let io = undefined;
/*
Here you can send web socket messages. 
Other files uses this method to inform about status changes.

web socket is started in index.js 
*/ 
exports.start = function (socket){
    io = socket;
    io.sockets.send('Started');
    console.log('Started');
}

exports.sendNotification = function (message){
    message.assetId = message.entityId;
    sendMessage(message);
}

async function sendMessage(message){
    console.log("message: "+JSON.stringify(message));
    io.sockets.send(message);
}
