/* 
This is the body of the newly activated websocket message:

-assetName is the name of the asset
-alarmName is the name of the threshold what is created
-sendTo was supposed to be the name of the person/group who would receive the web socket message. This feature is not in use.
-threshold contains the value of the threshold
-type has two states OVER or UNDER which defines will the threshold be activated when value is over or under the threshold
-status holds the status of the job which are NORMAL, UNSOLVED, PENDING, FIXED
-entityId is assets ID
-aspectName is aspects name
-variableName is variables name
*/
class AlarmMessage {
    
    constructor(assetName,name,sendTo,threshold,type,entityId,aspectName,variableName,status) {
        this.assetName = assetName;
        this.alarmName = name;
        this.sendTo = sendTo;
        this.threshold = threshold;
        this.type = type;
        this.status = status;
        this.entityId =entityId;
        this.aspectName = aspectName;
        this.variableName = variableName;
    }
    createMessage(){
        let parameter = {
            assetName: this.assetName,
            name: this.alarmName,
            sendTo: this.sendTo,
            threshold: this.threshold,
            type: this.type,
            entityId: this.entityId,
            aspectName: this.aspectName,
            variableName: this.variableName,
            status: this.status
        }
        return parameter;
    }
}

module.exports = AlarmMessage;