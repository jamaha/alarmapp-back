# WorkFlow back-end
This repo contains WorkFlow application node + express back-end which offers API for the Web application front-end. This is Project done by team JAMAHA in Metropolia innvation project -course. [Front-end is in separate repository.](https://gitlab.com/jamaha/alarmapp-front)
## Description
WorkFlow is an application that is intended for plant workers. It gives out status information and alerts about the plant machines. 

WorkFlow is created as extension of Mindsphere platform. The application uses Mindsphere’s APIs to access machine data. The accessable data is in timeseries format and describes the values of different measured variables and their fluctuations. 

WorkFlow application itself consists of front-end web application and Node server. Node server works as a intermidiate step between Mindsphere APIs and front-end as data filter. In addition the server handles thresholds, status and status logs. Data to the front-end is mainly provided through http requests, but more time critical information is provided through websocket. For example status changes. 

WorkFlow front-end uses React and Material-UI libraries together to provide dynamic interface. Data from back-end is stored in Redux store where it is displayed through the React components.


![Archtecture diagram](doc/architecture-final.svg)

## Data flow
![Dataflow diagram](doc/data-flow.png)