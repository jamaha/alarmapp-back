'use strict';
const express = require('express');
const router = express();
const TSaction = require('./controllers/timeSeriesController');
const assets = require('./controllers/assetController');
const fileController = require('./controllers/fileController');
const FileManager = require('./controllers/FileManager');
const sm = require('./websocket/streamManager');
const conf = require('./controllers/configure/customer-config');
const taskManager = require('./logic/taskManager');
const Status =require('./models/Status');
const fm = require('./controllers/FileManager');
/*
You can call with http://localhost:3000/alarmApi/*

Example: http://localhost:3000/alarmApi/assets/aspect?id=core.sinumerikbasicconfig
*/
//--------------------------------------------TIMESERIES
router.get('/timeseries/latest', TSaction.getLatestTimeSeries);
router.get('/timeseries/current', TSaction.getCurrentValue);
// keys {eId, pName, aName, from, to, iValue, iUnit} '/timeseries/aggregate?eid=8815ea76df054befb60d21408ad4e27e&pName=Port1&from=2019-11-09T10:11:00Z&to=2019-11-09T11:11:00Z&aName=Energy_consumpt_meter&intervalUnit=minute&intervalValue=5
// requires eId, pName at least. Default gives last hour of data from given property set. Intervalunits can be 'day', 'hour', 'minute' and intervalunit refers to how many of those in given from-to
// from-to must be given at same accuracy as intervalunit (eg. iUnit='minute', from and to have to be whole minutes '2019-11-09T11:11:00Z') and from-to must be 
// divisible by intervalvalue (eg. 60 min can be divided into 5 min intervals5 as 60 % 5 = 0)
router.get('/timeseries/aggregate', TSaction.getAggregatedValue);

//----------------------------------------------USED
router.get('/assets', assets.getListOfAssets);
//keys {id,eid} query: '/assets/type?id=asdasdasdasdasd...'&eid=asd867asd67....
router.get('/asset',assets.getAssetWithId);

router.get('/assettype', assets.getAssetTypeWithId);
//keys {id,eid,pName} query: '/assets/type?id=asdasdasdasdasd...'&eid=asd867asd67....&pName=C404
router.get('/variables', assets.getVariablesWithID);
router.get('/thresholds',function (req, res) {
	res.send(FileManager.getThresholds(req));
	res.end();
});
//-----------------------------------------------------FILES
router.get('/files', fileController.getFiles);
router.get('/file', fileController.getFileWithID);
if(conf.devMode){
	router.get('/file/search', fileController.findFile);
	router.get('/file/delete', fileController.deleteFileByName);
}

router.post('/file/create/logfile',function (req, res){
	fileController.createLogFile(conf.thresholdStorageId,"logfile",req, res);
});
router.post('/file/create/threshold',function (req, res){
	fileController.createTresholdFile(conf.thresholdStorageId,"thresholds",req, res);
});
router.post('/file/delete/threshold',function (req, res){
	fileController.deleteTresholdFile(conf.thresholdStorageId,"thresholds",req, res);
});

if(conf.devMode){
	router.post('/file/create',function (req, res){
		fileController.createFile(conf.thresholdStorageId,"test132",req, res);
	});
}
//------------------------------------------------------Socket test
if(conf.devMode){
	router.get('/wstest',function (req, res){
		sm.sendNotification("test","test");
		res.end();
	});
}
//-------------------------------------------------------Analyzer
router.post('/status/unsolved',function (req, res){
	taskManager.statusUpdate(req.body.name,req.body.entityId,req.body.aspectName,Status.UNSOLVED);
	res.end();
});
router.post('/status/pending',function (req, res){
	taskManager.statusUpdate(req.body.name,req.body.entityId,req.body.aspectName,Status.PENDING);
	res.end();
});
router.post('/status/fixed',function (req, res){
	taskManager.statusUpdate(req.body.name,req.body.entityId,req.body.aspectName,Status.FIXED);
	res.end();
});

if(conf.devMode){
	router.get('/test',function (req, res){	
		fm.testStuff(res);
		res.end();
	});
}

module.exports = router;