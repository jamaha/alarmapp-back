/*
 alarmAnalyzer is the heart of the alarming system it compares the values to the threshold and infroms us of something is wrong
*/

let moment = require('moment');

const conf = require('../controllers/configure/customer-config');
const fileController = require('../controllers/fileController');
const sm = require('../websocket/streamManager');
const AlarmMessage = require('../websocket/alarmMessage');
const TSaction = require('../controllers/timeSeriesController');
const rUtil = require('./requireUtil');
const taskManager = require('./taskManager');

let storageID = conf.thresholdStorageId;
let storageName = conf.thresholdStorageName;

let timer = conf.alarmTimer;

let thresholdList = undefined;
let latestThresholdList = undefined;

//start is runned in index.js
exports.start = function () {
    //test();
    preConfigure();
}
exports.setLatestThresholds = function (latestList) {
    latestThresholdList = latestList;
    console.log("Latest threshold list is:" + JSON.stringify(latestThresholdList));
}
exports.getLatestThresholds = function () {
    return thresholdList;
}

//preConfigure is the configures what has to be done before running alarmInspector
async function preConfigure() {
    if (global.token != "") {
        console.log(storageID + " " + storageName);
        let temp = await fileController.getStorageFile(storageID, storageName, );
        if (temp == undefined) {
            temp = [];
        }
        thresholdList = temp;
        console.log("Alarms will be checked every " + timer + " MS");
        console.log(thresholdList);
        alarmInspector();
    } else {
        setTimeout(preConfigure, 2000);
    }

}

/*
alarmInspector gets the values based on the threshold content and compares them. It also checks if it has notified the job from taskManager. 
After all it gets the latest thresholds list from MindSphere file system.
The alarmInspector is runned by every 30 sec. It is possible to change the value in customer-config  -> alarmTimer.
*/ 
async function alarmInspector() {
    let value = undefined;
    let jobStatus = undefined;
    //let jobHasChanged = taskManager.hasJobsChanged();
    for (let th of thresholdList) {

        console.log(th.entityId + " " + th.aspectName + " " + th.variableName);
        value = await TSaction.returnCurrent(th.entityId,th.aspectName,th.variableName);
        console.log("VALUE WAS: ",value);
        //use dummy(); to create fake data 
        if (value != undefined) {
            if (compairValues(value[th.variableName], th)) {
                
                jobStatus = taskManager.checkStatusList(th.name, th.entityId, th.aspectName);
                
                if(taskManager.hasJobsChanged() && !jobStatus.notified){
                    let assetName = await rUtil.getAssetController().returnAssetWithId(th.entityId);
                    if(assetName.name == undefined){
                        assetName.name = "empty";
                    }
                    console.log(jobStatus.status);
                    console.log("ALARM ALARM ALARM");
                    console.log();
                    sm.sendNotification(createMessage(assetName.name,th.name,
                        th.sendTo,
                        th.threshold,
                        th.type,
                        th.entityId,
                        th.aspectName,
                        th.variableName,
                        jobStatus.status));
                }
            }
        }
    }
    let temp = await fileController.getStorageFile(storageID, storageName, );
    if (temp == undefined) {
        temp = [];
    }
    thresholdList = temp;
    console.log();
    console.log("Amount of thresholds: "+thresholdList.length);
    console.log();
    console.log("Latest threshold list is:" + JSON.stringify(thresholdList));
    console.log();
    setTimeout(alarmInspector, timer);
}


function compairValues(value, th) {
    if (th.active == "ALL") {
        return compairHours(value, th);
    } else {
        let active = moment(th.active);
        let expire = moment(th.expire);
        let dateBefore = moment().isBefore(expire);
        let dateAfter = moment().isAfter(active);

        if (dateBefore && dateAfter) {
            return compairHours(value, th);
        }
    }
    return false;
}


function compairHours(value, th) {
    
    if (th.start == "ALL") {
        return compairThresholds(value, th);
    } else {
        let h = moment(th.start, "HH:mm:ss").add(th.hours, "hours");
        let h2 = moment(th.start, "HH:mm:ss");
        let timeBefore = moment().isBefore(h);
        let timeAfter = moment().isAfter(h2);

        if (timeBefore && timeAfter) {
            return compairThresholds(value, th);
        }
    }
}


function compairThresholds(value, th) {
    
    if (th.type == "OVER") {

        if (value > th.threshold) {
            console.log("value:" + value + " was OVER threshold:" + th.threshold);
            return true;
        } else {
            return false;
        }

    } else if (th.type == "UNDER") {

        if (value < th.threshold) {
            console.log("value:" + value + " was UNDER threshold:" + th.threshold);
            return true;
        } else {
            return false;
        }
    }
    return false;
}


function createMessage(assetName,name, sendTo, threshold, type, entityId, aspectName, variableName, status) {
    let params = new AlarmMessage(assetName,name, sendTo, threshold, type, entityId, aspectName, variableName, status).createMessage();
    return params;
}

//dummy is used when we want to test our application with mock data
function dummy() {
    let param = {
        temperature: 22.06,
        temperature_qc: 0,
        _time: '2019-11-19T23:11:20.929Z'
    }
    return param;
}

//test was used to test dummy threshold data
function test() {
    let param = {
        "name": "test voltage check",
        "entityId": "8815ea76df054befb60d21408ad4e27e",
        "aspectName": "Port1",
        "variableName": "Act_filtered_DC_link_volt",
        "start": "15:20:00",
        "hours": 0.25,
        "active": "2019-08-26T00:00:00Z",
        "expire": "2019-12-25T00:00:00Z",
        "threshold": 15.30938721,
        "sendTo": "worker1",
        "type": "OVER"
    }
    compairValues(50, param);
    param = {
        "name": "test voltage check",
        "entityId": "8815ea76df054befb60d21408ad4e27e",
        "aspectName": "Port1",
        "variableName": "Act_filtered_DC_link_volt",
        "start": "ALL",
        "hours": 0,
        "active": "ALL",
        "expire": "2019-12-25T00:00:00Z",
        "threshold": 15.30938721,
        "sendTo": "worker1",
        "type": "OVER"
    }
    compairValues(50, param);
    param = {
        "name": "test voltage check",
        "entityId": "8815ea76df054befb60d21408ad4e27e",
        "aspectName": "Port1",
        "variableName": "Act_filtered_DC_link_volt",
        "start": "16:00:00",
        "hours": 3,
        "active": "2019-08-26T00:00:00Z",
        "expire": "2019-12-25T00:00:00Z",
        "threshold": 55.30938721,
        "sendTo": "worker1",
        "type": "UNDER"
    }
    compairValues(50, param);
}