/*
In taskManager we manage all the jobs what has been created when alarm has been activated.
*/
const JobStatus = require('./jobStatus');
const Status = require('../models/Status');
const moment = require('moment');
const sm = require('../websocket/streamManager');
const rUtil = require('./requireUtil');
//const AlarmMessage = require('../websocket/alarmMessage');

//jobs is a list which contains all the jobStatus models in it.
let jobs = [];
let jobCount = 0;

// statusUpdate used in API and changeStatus changes the status of the job (UNSOLVED,PENDING,FIXED)
exports.statusUpdate = function (name, entityId, aspectName, status) {
    changeStatus(name, entityId, aspectName, status);
}
//hasJobsChanged used to verify if there is new job. Used in alarmAnalyzer when we want to know if it should send web socket message or not.
exports.hasJobsChanged = function () {
    if(jobs.length != jobCount){
        if(jobs.length > jobCount){
            return true;
        }
        jobCount = jobs.length;
    }
    return false;
}

//getStatus returns status of the job by searchin jobs list with id which is id-{entityId}{aspectName}
exports.getStatus = function (name, entityId, aspectName) {
    //let id = "id-"+name+entityId+aspectName;
    let id = "id-" + entityId + aspectName;
    try {
        for (let i = 0; i < jobs.length; i++) {
            if (id == jobs[i].getId()) {

                return jobs[i].getStatus().name;

            } else {

            }
        }
    } catch (err) {
        console.log(err)
    }
    return Status.NORMAL.name;
}
//checkStatus creates and deletes the jobs from jobs list. Returns the status and notified value which is used when we want to know if job has been alarmed already. Called in alarmAnalyzer.
exports.checkStatusList = function (name, entityId, aspectName) {
    //let id = "id-"+name+entityId+aspectName;
    let id = "id-" + entityId + aspectName;
    console.log("jobs " + (jobs.length));
    let latestJobs = [];
    let statusList = [];
    let returnValues = {
        status: 'UNSOLVED',
        notified: true
    }
    try {
        for (let i = 0; i < jobs.length; i++) {
            //console.log(jobs[i].getId());
            if (id == jobs[i].getId()) {
                returnValues.notified = jobs[i].hasJobBeenNotified();
                statusList.push(jobs[i].getStatus());
            }
            if(jobs[i].getExpireTime() == undefined){
                latestJobs.push(jobs[i]);
            }
            else if(moment().isBefore(jobs[i].getExpireTime())){
                latestJobs.push(jobs[i]);
            }else{
                console.log(i+" JOB WAS REMOVED");
            }
        }
        jobs = latestJobs;
        returnValues.status = priorityChechk(statusList)
        if(returnValues.status !== "NORMAL"){
            console.log("RETURNED STATUS "+returnValues.status);
            return returnValues;      
        }
    } catch (err) {
        console.log(err)
    }
    jobs.push(new JobStatus(name, entityId, aspectName));
    returnValues.status = Status.UNSOLVED.name;
    return returnValues;
}
//priorityChechk checks the Status models level and decides which status should be used
function priorityChechk(statusList){
    
    let priority = 0;
    let status = Status.NORMAL;
    for(let s of statusList){
        if(s.level > priority){
            status = s;
            priority = status.level;
        }
    }

    return status.name;
}

async function changeStatus(name, entityId, aspectName, status) {
    //let id = "id-"+name+entityId+aspectName;
    let id = "id-" + entityId + aspectName;
    let assetName = await rUtil.getAssetController().returnAssetWithId(entityId);
    
    if(assetName.name == undefined){
        assetName.name = "empty";
    }
    for (let i = 0; i < jobs.length; i++) {
        if (id == jobs[i].getId()) {
            jobs[i].setStatus(status);
            sm.sendNotification(
                {assetName: assetName.name,
                entityId: entityId,
                aspectName: aspectName,
                status: status.name});

        }
    }
}

