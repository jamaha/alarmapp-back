/*
JobStatus is the model which holds all the data about created jobs what are created when alarm is activated.

-alarmName name of the saved threshold

-expire contains the value when the job should be deleted from the jobs list.
-removeStatus not used
-notified tells to alarmAnalyzer if it has been alarmed (sent web socket message) or not.
*/
let Status = require('../models/Status');
let moment = require('moment');
const conf = require('../controllers/configure/customer-config');

class JobStatus {

    constructor(name, entityId, aspectName) {
        this.alarmName = name;
        this.entityId = entityId;
        this.aspectName = aspectName;
        this.status = Status.UNSOLVED;
        this.expire = undefined;
        this.removeStatus = false;
        this.notified = false;
    }
    //setStatus updates the status and if the status is FIXED it will put a expire date which can be configured in customer-config file
    setStatus(s) {
        console.log("new status is " + s.name);
        this.status = s;
        if(s.name == "FIXED"){
         console.log(this.alarmName+" can be removed");
            this.expire =  moment().add(conf.cleanTimerInMinutes, 'minutes');
        }
    }
    getId() {
        //return "id-"+this.alarmName+this.entityId+this.aspectName;
        return "id-" + this.entityId + this.aspectName;
    }
    getStatus() {
        return this.status;
    }
    getExpireTime(){
        return this.expire
    }
    hasJobBeenNotified(){
        let returnStatus = this.notified;
        this.notified = true;
        return returnStatus;
    }
}

module.exports = JobStatus;