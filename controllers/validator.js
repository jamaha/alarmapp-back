//Validator is used for validating input data from REST API. Used for saving thresholds and logs.

let moment = require('moment');
let crypto = require('crypto');

exports.validateThresholdContent = function (d) {
    let raport = {
        status: true,
        message: "success"
    }
    if (basicCheck(d)) {

        data = setDefaultValuesForData(d);
        
        if (data.name == undefined) {
            raport.message = "No name found";
            raport.status = false;
            return raport;
        }
        if (data.entityId == undefined) {
            raport.message = "No entityId found";
            raport.status = false;
            return raport;
        }
        if (data.aspectName == undefined) {
            raport.message = "No aspectName found";
            raport.status = false;
            return raport;
        }
        if (data.variableName == undefined) {
            raport.message = "No variableName found";
            raport.status = false;
            return raport;
        }
        if (data.start == undefined) {
            raport.message = "No start found";
            raport.status = false;
            return raport;
        }
        if (data.hours == undefined) {
            raport.message = "No hours found";
            raport.status = false;
            return raport;
        }
        if (data.active == undefined) {
            raport.message = "No active found";
            raport.status = false;
            return raport;
        }
        if (data.expire == undefined) {
            raport.message = "No expire found";
            raport.status = false;
            return raport;
        }
        if (data.threshold == undefined) {
            raport.message = "No threshold found";
            raport.status = false;
            return raport;
        }
        if (data.sendTo == undefined) {
            raport.message = "No sendTo found";
            raport.status = false;
            return raport;
        }
        if (data.type == undefined) {
            raport.message = "No type found";
            raport.status = false;
            return raport;
        }
        if (typeof data.name !== 'string') {
            raport.message = "name must be string";
            raport.status = false;
            return raport;
        }
        if (typeof data.entityId !== 'string') {
            raport.message = "entityId must be string";
            raport.status = false;
            return raport;
        }
        if (typeof data.aspectName !== 'string') {
            raport.message = "aspectName must be string";
            raport.status = false;
            return raport;
        }
        if (typeof data.variableName !== 'string') {
            raport.message = "variableName must be string";
            raport.status = false;
            return raport;
        }
        if (typeof data.start !== 'string') {
            raport.message = "start must be string";
            raport.status = false;
            return raport;
        }
        if (typeof data.type !== 'string') {
            raport.message = "type must be string";
            raport.status = false;
            return raport;
        }
        if (typeof data.hours !== 'number') {
            raport.message = "hours must be number";
            raport.status = false;
            return raport;
        }
        if (typeof data.threshold !== 'number') {
            raport.message = "threshold must be number";
            raport.status = false;
            return raport;
        }
        if (data.name === null || data.name === "null" || data.name.length < 1) {
            raport.message = "name must contain something";
            raport.status = false;
            return raport;
        }
        if (data.entityId === null || data.entityId === "null" || data.entityId.length < 1) {
            raport.message = "entityId must contain something";
            raport.status = false;
            return raport;
        }
        if (data.aspectName === null || data.aspectName === "null" || data.aspectName.length < 1) {
            raport.message = "aspectName must contain something";
            raport.status = false;
            return raport;
        }
        if (data.variableName === null || data.variableName === "null" || data.variableName.length < 1) {
            raport.message = "variableName must contain something";
            raport.status = false;
            return raport;
        }
        if (data.start === null || data.start === "null" || data.start.length < 1) {
            raport.message = "start must contain something";
            raport.status = false;
            return raport;
        }
        if (data.type === null || data.type === "null" || data.type.length < 1) {
            raport.message = "type must contain something";
            raport.status = false;
            return raport;
        }
        if (data.type == "OVER" || data.type == "UNDER") {

        } else {
            raport.message = "type must contain OVER or UNDER";
            raport.status = false;
            return raport;
        }
        if (data.hours > 24) {
            data.start = "ALL";
        }

        if(data.id === null || data.id === "null" || data.id == undefined){
            let temp = data.name + moment().format();
            data.id = crypto.createHash('md5').update(temp).digest("hex");
        }

    } else {
        raport.message = "content was empty";
        raport.status = false;
    }
    return raport;
};
exports.validateThresholdId = function (data) {
    let raport = {
        status: true,
        message: "success"
    }
    if (basicCheck(data)) {

    }else {
        raport.message = "content was empty";
        raport.status = false;
    }

    if(data.id === null || data.id === "null" || data.id == undefined){
        raport.message = "id was undefined";
        raport.status = false;
        
    }
    return raport;
}

exports.validateLogContent = function (data) {
    let raport = {
        status: true,
        message: "success"
    }
    

    if (basicCheck(data)) {
        data.date = moment().format();

        if (data.name == undefined) {
            raport.message = "No name found";
            raport.status = false;
            return raport;
        }
        if (data.entityId == undefined) {
            raport.message = "No entityId found";
            raport.status = false;
            return raport;
        }
        if (data.aspectName == undefined) {
            raport.message = "No aspectName found";
            raport.status = false;
            return raport;
        }
        if (data.date == undefined) {
            raport.message = "No date found";
            raport.status = false;
            return raport;
        }
        if (data.desc == undefined) {
            raport.message = "No desc found";
            raport.status = false;
            return raport;
        }

        if (typeof data.name !== 'string') {
            raport.message = "name must be string";
            raport.status = false;
            return raport;
        }
        if (typeof data.entityId !== 'string') {
            raport.message = "entityId must be string";
            raport.status = false;
            return raport;
        }
        if (typeof data.aspectName !== 'string') {
            raport.message = "aspectName must be string";
            raport.status = false;
            return raport;
        }
        if (typeof data.date !== 'string') {
            raport.message = "date must be string";
            raport.status = false;
            return raport;
        }
        if (typeof data.desc !== 'string') {
            raport.message = "desc must be string";
            raport.status = false;
            return raport;
        }
        if (data.name === null || data.name === "null" || data.name.length < 1) {
            raport.message = "name must contain something";
            raport.status = false;
            return raport;
        }
        if (data.entityId === null || data.entityId === "null" || data.entityId.length < 1) {
            raport.message = "entityId must contain something";
            raport.status = false;
            return raport;
        }
        if (data.aspectName === null || data.aspectName === "null" || data.aspectName.length < 1) {
            raport.message = "aspectName must contain something";
            raport.status = false;
            return raport;
        }
        if (data.date === null || data.date === "null" || data.date.length < 1) {
            raport.message = "date must contain something";
            raport.status = false;
            return raport;
        }
        if (data.desc === null || data.desc === "null" || data.desc.length < 1) {
            raport.message = "desc must contain something";
            raport.status = false;
            return raport;
        }
        

    } else {
        raport.message = "content was empty";
        raport.status = false;
    }
    return raport;
};


function setDefaultValuesForData(data) {
    data.active = "ALL";
    data.expire = "";
    data.sendTo = "";

    return data;
}


function basicCheck(data) {
    if (data == undefined) {
        return false;
    }
    if (typeof data !== 'object') {
        return false;
    }
    return true
}