'use strict';

const ClientConfig = require('mindsphere-sdk-node-core').ClientConfig;
const UserCredentials = require('mindsphere-sdk-node-core').UserCredentials;

// Require TimeseriesClient from `timeseries-sdk` module
const TimeSeriesClient = require('timeseries-sdk').TimeSeriesClient;

// Require AggregatesClient from 'tsaggregates-sdk' module
const AggregatesClient = require('tsaggregates-sdk').AggregatesClient

// Proxy value can be set in the ClientConfig object
let config = new ClientConfig();

// Construct the UserCredentials object


exports.getLatestTimeSeries = function(_req, _res) {
    let eID = _req.query.eid;
    let pName = _req.query.pName;
    getLatestTS(eID, pName, _res);
};


async function getLatestTS(eId, pName, _res) {
    // Function for getting latest values for propertyname
    let config = new ClientConfig();
    let credentials = new UserCredentials({ 'authorization': global.token });
    let time_series_client = new TimeSeriesClient(config, credentials);

    try {
        let timeseries_data;
        const request_object = {
            entity: eId,
            propertysetname: pName,
            latestvalue: true
        };
        timeseries_data = await time_series_client.getTimeseries(request_object);
        console.log(timeseries_data);
        _res.write(timeseries_data);
    } catch (err) {
        console.log("errrrr " + err);
        _res.write(err);
    }
    _res.end();
}


exports.getCurrentValue = function(_req, _res) {
    let eId = _req.query.eid;
    let pName = _req.query.pName;
    let aName = _req.query.aName;
    getCurrentValueTS(eId, pName, aName, _res);
};


async function getCurrentValueTS(eId, pName, variableName, _res) {
    // Get latest value from last 10 minutes for given variable in property
    let ts = Date.now();
    let to = new Date(ts).toISOString();
    let from = new Date(ts - (600000)).toISOString();

    let timeseries_data;
    let config = new ClientConfig();
    let credentials = new UserCredentials({ 'authorization': global.token });
    let time_series_client = new TimeSeriesClient(config, credentials);

    try {
        const request_object = {
            entity: eId,
            propertysetname: pName,
            from: from,
            to: to,
            select: variableName,
            //sort: 'desc', // seems not to work for sorting last first
            limit: 1000
        };
        timeseries_data = await time_series_client.getTimeseries(request_object);
        let timeseriesParsed = JSON.parse(timeseries_data)
        if (variableName != undefined) {
            let currentValue = timeseriesParsed.slice(-1)[0];
            console.log(currentValue)
            _res.write(JSON.stringify(currentValue))
            console.log(timeseries_data)
        } else {
            let uniqueKeys = []
            let itemsToIterate = timeseriesParsed.slice(0).reverse();
            for (let prop in itemsToIterate) {
                if (uniqueKeys.includes(Object.keys(itemsToIterate[prop])[0])) {
                    delete itemsToIterate[prop]
                } else {
                    uniqueKeys.push(Object.keys(itemsToIterate[prop])[0]);
                }
            }
            let filtered = itemsToIterate.filter(function (el) {
                return el != null;
              });
            _res.write(JSON.stringify(filtered));
        }
    } catch (err) {
        console.log("errrrr " + err);
        _res.write(err);
        //Exception Handling
    }
    _res.end();
}

exports.getAggregatedValueForVariable = async function(entityid,aspectName,variableName) {
    let eId = entityid;
    let pName = aspectName;
    let aName = variableName;

    const roundDownTo = roundTo => x => Math.floor(x / roundTo) * roundTo;
    const roundDownToMinute = roundDownTo(1000*60);
    let ts = roundDownToMinute(Date.now())

    let to = new Date(ts).toISOString();
    let from = new Date(ts - 3600000).toISOString();
    let iUnit = 'minute';
    let iValue = 5;
    
    let aggregates_data;
    let config = new ClientConfig();
    let credentials = new UserCredentials({ 'authorization': global.token });
    let aggregates_client = new AggregatesClient(config, credentials);

    try {
        const request_object = {
            entity: eId,
            propertyset: pName,
            from: from,
            to: to,
            intervalValue : iValue,
            intervalUnit : iUnit,
            select: aName
        };
        //console.log(request_object)
        aggregates_data = await aggregates_client.getAggregateTimeseries(request_object);   
        let return_data = JSON.parse(aggregates_data);
        return_data.reverse();
        //console.log(JSON.stringify(return_data));
        return return_data;
        
    } catch (err) {
        console.log("Error in timeSeries",err);
    }
    return undefined;
};

exports.getAggregatedValue = function(_req, _res) {
    let eId = _req.query.eid;
    let pName = _req.query.pName;
    let aName = _req.query.aName;
    let from = _req.query.from;
    let to = _req.query.to;
    let iValue = _req.query.iValue;
    let iUnit = _req.query.iUnit;

    const roundDownTo = roundTo => x => Math.floor(x / roundTo) * roundTo;
    const roundDownToMinute = roundDownTo(1000*60);
    let ts = roundDownToMinute(Date.now())

    if(to == undefined){
        to = new Date(ts).toISOString();
    }
    if(from == undefined){
        from = new Date(ts - 3600000).toISOString();
    }
    if (iUnit == undefined){
        iUnit = 'minute'
        iValue = 5
    }
    getAggregatedTS(eId, pName, aName, from, to, iValue, iUnit,_res);
};


async function getAggregatedTS(eId, pName, aName, from, to, iValue, iUnit,_res){
    // Aggregated TS-data for variable or property set
    let aggregates_data;
    let config = new ClientConfig();
    let credentials = new UserCredentials({ 'authorization': global.token });
    let aggregates_client = new AggregatesClient(config, credentials);

    try {
        const request_object = {
            entity: eId,
            propertyset: pName,
            from: from,
            to: to,
            intervalValue : iValue,
            intervalUnit : iUnit,
            select: aName
        };
        //console.log(request_object)
        aggregates_data = await aggregates_client.getAggregateTimeseries(request_object);   
        //console.log(aggregates_data);
        _res.send(JSON.parse(aggregates_data));
        
    } catch (err) {
        console.log(err);
        //Exception Handling
    }
    _res.end()
}


exports.returnCurrent = async function (eId, pName, variableName) {
    let ts = Date.now();
    let to = new Date(ts).toISOString();
    let from = new Date(ts - (600000)).toISOString();

    let timeseries_data;
    let config = new ClientConfig();
    let credentials = new UserCredentials({ 'authorization': global.token });
    let time_series_client = new TimeSeriesClient(config, credentials);
    
    try {
        const request_object = {
            entity: eId,
            propertysetname: pName,
            from: from,
            to: to,
            select: variableName,
            //sort: 'desc', // seems not to work for sorting last first
            limit: 1000
        };
        timeseries_data = await time_series_client.getTimeseries(request_object);
        let timeseriesParsed = JSON.parse(timeseries_data)
        if (variableName != undefined) {
            let currentValue = timeseriesParsed.slice(-1)[0];
            //console.log(currentValue)
            return currentValue;
        } else {
            let uniqueKeys = []
            //console.log(timeseriesParsed)
            //console.log(Object.keys(timeseriesParsed))
            let itemsToIterate = timeseriesParsed.slice(0).reverse();
            for (let prop in itemsToIterate) {
                if (uniqueKeys.includes(Object.keys(itemsToIterate[prop])[0])) {
                    delete itemsToIterate[prop]
                } else {
                    uniqueKeys.push(Object.keys(itemsToIterate[prop])[0]);
                    //console.log(itemsToIterate[prop])
                }
            }
            let filtered = itemsToIterate.filter(function (el) {
                //console.log(el)
                return el != null;
              });
        }
    } catch (err) {
        console.log("errrrr " + err);
        //Exception Handling
    }
}