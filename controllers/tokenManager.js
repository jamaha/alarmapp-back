/*
tokenUpdater updates the token every x minutes. This is used in local development.
*/
let request = require("request");
global.token = "";

module.exports = {
    start: function () {
        startTokenUpdate();
    }
}

async function startTokenUpdate() {

    tokenUpdater();
}

function tokenUpdater() {
    const raw = 'mpamkdev-tokenapp-1.0.0:7PySrkEDZK6xQGoNyEQEqkiohBQts4hGg5bvH51HTFn';
    const hash = 'Basic ' + Buffer.from(raw).toString('base64');
    var options = {
        method: 'POST',
        url: 'https://gateway.eu1.mindsphere.io/api/technicaltokenmanager/v3/oauth/token',
        headers: {
            'Accept': '*/*',
            'Content-Type': 'application/json',
            'X-SPACE-AUTH-KEY': hash
        },
        body: {
            appName: 'tokenapp',
            appVersion: '1.0.0',
            hostTenant: 'mpamkdev',
            userTenant: 'mpamkdev'
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error) {
            setTimeout(tokenUpdater, 5000);
            //throw new Error(error);
            console.log(error);
        } else {
            console.log(body);
            let timeToWait = body.expires_in * 1000 - 50000;
            setTimeout(tokenUpdater, timeToWait);
            global.token = body.access_token;
            console.log(global.token);
        }
    });
}