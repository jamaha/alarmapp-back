/*
FileManager manages the saved data files like thresholds and logs.
*/

const analyzer = require('../logic/alarmAnalyzer');
const Thresholds = require('../models/Thresholds');


exports.getThresholds = function (_req){
    let thresholds = analyzer.getLatestThresholds();
    if(thresholds == undefined){
        return [];
    }else{

        return filterThresholds(_req.query.eid,_req.query.aName,_req.query.vName,thresholds)
    }

}

function filterThresholds(entityId,aspectName,variableName,thresholds) {
    let tempList = [];
    let filterId = entityId+aspectName+variableName;
    for (let t of thresholds){
        let tId = t.entityId+t.aspectName+t.variableName;
        if(filterId == tId){
            tempList.push(new Thresholds(t.id,t.name,
                t.entityId,
                t.aspectName,
                t.variableName,
                t.start,
                t.hours,
                t.threshold,
                t.type));
        }
    }
    return tempList;
}

exports.validateThresholdFile = function (name, newFile) {
    return validateThreshold(name, newFile);
};

exports.deleteThreshold = function (name, newFile) {
    return deleteThresholdWithId(name, newFile);
};


exports.validateLogFile = function (name, fileList, newFile) {
    return validateLog(name, fileList, newFile);
};

//Finds the threshold with id and updates the values if the threshold is not in the list it will create new one.
function validateThreshold(name, newFile) {
    let finalFile = undefined;
    if (name == "thresholds") {
        let thresholds = analyzer.getLatestThresholds();
        let newId = "id-" + newFile.id;
        let id = undefined;

        for (let i = 0; i < thresholds.length; i++) {
            id = "id-" + thresholds[i].id;
            if (newId == id) {
                thresholds[i] = newFile;
                analyzer.setLatestThresholds(thresholds);//migth be usless function
                return thresholds;
            }
        }
        thresholds.push(newFile);
        analyzer.setLatestThresholds(thresholds);//migth be usless function
        finalFile = thresholds;
    } else {
        finalFile = [];
    }
    return finalFile;
}
//Finds the threshold with id and removes it
function deleteThresholdWithId(name, newFile) {
    let newThresholds = [];
    if (name == "thresholds") {
        let thresholds = analyzer.getLatestThresholds();
        let newId = "id-" + newFile.id;
        let id = undefined;

        for (let i = 0; i < thresholds.length; i++) {
            id = "id-" + thresholds[i].id;
            if (newId == id) {
                
            }else{
                newThresholds.push(thresholds[i]);
            }
        }
        
    } 
    return newThresholds;
}
//checks if log list exist already and adds the log to the list
//if the list has not been created it will create it and add the first log
function validateLog(name, fileList, newFile) {
    let finalFile = undefined;
    if (fileList == undefined) {
        fileList = [];
        fileList.push(newFile);
        finalFile = fileList;
    } else {
        fileList.push(newFile);
        finalFile = fileList;
    }
    return finalFile;

}


function validate(name, fileList, newFile) {
    let finalFile = undefined;
    if (name == "thresholds") {
        let thresholds = analyzer.getLatestThresholds();
        let newId = "id-" + newFile.entityId + newFile.aspectName + newFile.variableName;
        let id = undefined;

        for (let i = 0; i < thresholds.length; i++) {
            id = "id-" + thresholds[i].entityId + thresholds[i].aspectName + thresholds[i].variableName;
            if (newId == id) {
                thresholds[i] = newFile;
                analyzer.setLatestThresholds(thresholds);
                return thresholds;
            }
        }
        thresholds.push(newFile);
        analyzer.setLatestThresholds(thresholds);
        finalFile = thresholds;
    } else if (name == "logfile") {
        name = "file" + newFile.entityId + newFile.aspectName + name;
        fileList.push(newFile);
        finalFile = fileList;
    } else {
        finalFile = [];
    }
    return finalFile;
}