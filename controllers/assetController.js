'use strict';

const util = require('./helper');
let RequestManager = require('./requestManager');
// Require AppCredentials and ClientConfig from mindsphere-sdk-node-core module
const UserCredentials = require('mindsphere-sdk-node-core').UserCredentials;
let ClientConfig = require('mindsphere-sdk-node-core').ClientConfig;

// Require AspecttypeClient from assetmanagement-sdk module
const AssettypeClient = require('assetmanagement-sdk').AssettypeClient;
const AssetsClient = require('assetmanagement-sdk').AssetsClient;
const modelController = require('./modelController');

let asset_path = util.getAssetPath();
let token = "";
// Construct the UserCredentials object

//USED
exports.getAssetTypeWithId = async function (_req, _res) {
    token = util.updateToken();
    let config = new ClientConfig();
    let credentials = new UserCredentials({
        'authorization': token
    });
    let asset_type_client = new AssettypeClient(config, credentials);

    try {
        let request_object = {
            id: _req.query.id + "?includeShared=true"
        };
        let asset_types = await asset_type_client.getAssetType(request_object);
        let response = modelController.createFilteredAspect(_req.query.eid, asset_types);
        _res.send(response);
    } catch (err) {
        console.log("errrrr " + err);
        _res.end();
    }
    _res.end();
}


exports.getVariablesWithID = async function (_req, _res) {
    token = util.updateToken();
    let config = new ClientConfig();
    let credentials = new UserCredentials({
        'authorization': token
    });
    let asset_type_client = new AssettypeClient(config, credentials);

    try {
        let request_object = {
            id: _req.query.id + "?includeShared=true"
        };
        let asset_types = await asset_type_client.getAssetType(request_object);
        let response = await modelController.createFilteredVariable(_req.query.eid, _req.query.pName, asset_types);
        _res.send(response);
    } catch (err) {
        console.log("errrrr " + err);
        _res.end();
    }
    _res.end();
}


//USED  
exports.getListOfAssets = async function (_req, _res) {
    token = util.updateToken();
    let rpage = _req.query.page;
    let rsize = _req.query.size;
    let rsort = _req.query.sort;
    let rfilter = _req.query.filter;
    let rifNoneMatch = _req.query.ifNoneMatch;

    let rm = new RequestManager();
    let param = {
        includeShared: 'true',
        page: rpage,
        size: rsize,
        sort: rsort,
        filter: rfilter,
        ifNoneMatch: rifNoneMatch
    };
    let raw_content = await rm.returnFromMS(asset_path, param);

    console.log('raw_content', raw_content)
    let response = modelController.createFilteredAsset(raw_content);
    if (response.status != 200) {
        _res.send(response);
        _res.end();
    } else {
        _res.send(response);
        _res.end();
    }
};

exports.getAssetWithId = async function (_req, _res) {
    let id = _req.query.eid + "?includeShared=true";
    _res.send(await getAsset(id));
    _res.end();
  
  };

exports.returnAssetWithId = async function (eid) {
    let id = eid + "?includeShared=true";
    let value = await getAsset(id);
    return JSON.parse(value);
  
};

  async function getAsset(id) {
        token = util.updateToken();
        let config = new ClientConfig();
        let credentials = new UserCredentials({ 'authorization': token });
        let asset_client = new AssetsClient(config, credentials);
        try {
            let request_object = {
                id: id
            };
            let asset_types  = await asset_client.getAsset(request_object);
            
            return asset_types;
        } catch (err) {
            console.log("errrrr " + err);
        }
        return undefined;
    }
    