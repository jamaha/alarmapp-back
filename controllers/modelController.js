
/**
 * Model controller filters data what comes from MindSphere
 */

const Assets = require('../models/Assets');
const Aspects = require('../models/Aspects');
const Variables = require('../models/Variables');
const taskManager = require('../logic/taskManager');
const TSaction = require('./timeSeriesController');

let response = {
    status: 200,
    content: undefined,
    message: "success"
}


exports.createFilteredAsset = function (raw_content) {
    if (raw_content == undefined) {
        response.status = 500;
        response.content = raw_content;
        response.message = "Asset was undefined";
        return response;
    }
    let assets = [];
    for (let value of raw_content) {
        assets.push(new Assets(value.assetId, value.name, value.typeId));
    }
    response.content = assets;
    return response;
}


exports.createFilteredAspect = function (assetId, raw_content) {
    if (raw_content == undefined) {
        response.status = 500;
        response.content = raw_content;
        response.message = "Aspect was undefined";
        return response;
    }
    let debug1 = undefined;
    let debug2 = undefined;
    let content = [];
    let json_content = JSON.parse(raw_content);
    console.log(json_content);

    for (let aspect of json_content.aspects) {

        debug1 = aspect;
        content.push(new Aspects(assetId, aspect.name, taskManager.getStatus("", assetId, aspect.name)));
    }
    debug2 = json_content.aspects;
    console.log(debug1);
    console.log();
    console.log(debug2);
    response.content = content;

    return response;
}


exports.createFilteredVariable = async function (assetId, aspectName, raw_content) {
    if (raw_content == undefined) {
        response.status = 500;
        response.content = raw_content;
        response.message = "variables was undefined";
        return response;
    }
    let content = [];
    let json_content = JSON.parse(raw_content);
    console.log(json_content);
    let value = undefined;
    for (let aspect of json_content.aspects) {
        for (let variable of aspect.aspectType.variables) {
            if (aspectName == aspect.name) {
                let values = await TSaction.getAggregatedValueForVariable(assetId, aspectName,variable.name);
                value = undefined;
                if(values != undefined){
                    for(let v of values){
                        if(v[variable.name] != undefined){
                            value = v[variable.name].lastvalue;
                            console.log(value);
                            break;
                        }
                    }
                    
                }
                content.push(new Variables(assetId, aspectName, variable.name, value,variable.unit, taskManager.getStatus("", assetId, aspect.name)));
            }
        }
    }
    response.content = content;
    return response;
}