"use strict";
/*

*/
var request = require("request");
let util = require('./helper');

function refineAssets(assets) {
    let refinedAssets = assets['_embedded']['assets']
    for (const asset of refinedAssets) {
        for (const key of Object.keys(asset)) {
            //console.log(key)
            if (['assetId', 'name', 'typeId'].includes(key)) {
                //console.log(key)
            } else {
                delete asset[key]
            }
        }
    }
    return refinedAssets;
}

/**
 * @class RequestManager
 * @param {JSON} RequestManager json object with keys proxy and timeout
 * Because we had some problems to get all data even shared we decided to create our own way to call MindSphere
 * */
class RequestManager {

    constructor() {
        this.token = util.updateToken();
    }
    //Params: { includeShared: 'true' }
    async getFromMS(destination, params, _res) {

        let gateway = util.getDestination();
        console.log(gateway + destination);
        var options = {
            method: 'GET',
            url: gateway + destination,
            qs: params,
            headers: {
                'cache-control': 'no-cache',
                Connection: 'keep-alive',
                'Cache-Control': 'no-cache',
                Accept: '*/*',
                Authorization: 'Bearer ' + this.token
            }
        };

        request(options, function (error, response, body) {
            if (error) {
                return error;
            }
            let assets = JSON.parse(body)
            let refinedAssets = assets['_embedded']['assets']
            console.log(refinedAssets)
            for (const asset of refinedAssets) {
                for (const key of Object.keys(asset)) {
                    //console.log(key)
                    if (['assetId', 'name', 'typeId'].includes(key)) {
                        //console.log(key)
                    } else {
                        delete asset[key]
                    }
                }
            }
            _res.send(JSON.stringify(refinedAssets));
        });
    }


    async returnFromMS(destination, params) {

        let gateway = util.getDestination();
        console.log(gateway + destination);
        var options = {
            method: 'GET',
            url: gateway + destination,
            qs: params,
            headers: {
                'cache-control': 'no-cache',
                Connection: 'keep-alive',
                'Cache-Control': 'no-cache',
                Accept: '*/*',
                Authorization: 'Bearer ' + this.token
            }
        };

        return new Promise((resolve, reject) => {
            try {
                request(options, (error, response, body) => {
                    const assets = JSON.parse(body);
                    resolve(refineAssets(assets));
                });
            } catch (error) {
                console.error('Error on returnFromMS', error);
                reject();
            }
        })
    }


    async postToMS(destination, params, header, body, _res) {

        let gateway = util.getDestination();
        console.log(gateway + destination);
        try {
            var options = {
                method: 'PUT',
                url: gateway + destination,
                qs: params,
                headers: header,
                body: body
            };

            request(options, function (error, response, body) {
                if (error) {
                    console.log(error);
                    return error;
                }
                _res.write(body);
                _res.end();
            });
        } catch (err) {
            res.end();
            console.log("ERROR in requestManager posttoms",err);
            throw err;
        }
    }
}

module.exports = RequestManager;