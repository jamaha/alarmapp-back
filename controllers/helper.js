"use strict";
/*
Helper is designed to be handy tool what helps with different things.
For now it is used for fetching data from customer-config file.

*/

module.exports = {
    updateToken: function () {
        return global.token;
    },

    getDestination: function () {
        let obj = require('./configure/customer-config.json');
        return obj.destination;
    },

    getAssetPath: function () {
        let obj = require('./configure/customer-config.json');
        return obj.assetPath;
    },

    getAspectPath: function () {
        let obj = require('./configure/customer-config.json');
        return obj.aspectPath;
    },
    getAssetTypePath: function () {
        let obj = require('./configure/customer-config.json');
        return obj.assetTypePath;
    },

    getFilePath: function () {
        let obj = require('./configure/customer-config.json');
        return obj.files;
    }
}