// Import the mindsphere-sdk-node-core module and require AppCredentials and ClientConfig
const UserCredentials = require('mindsphere-sdk-node-core').UserCredentials;
let ClientConfig = require('mindsphere-sdk-node-core').ClientConfig;

// Import the iotfileservices module and require FileServiceClient
const FileServiceClient = require('iotfileservices-sdk').FileServiceClient;
const util = require('./helper');
const fm = require('./FileManager');
const validator = require('./validator');

//-------------------------------------------------------Search File

exports.findFile = function (_req, _res) {
  let eID = _req.query.eid;
  let fpath = _req.query.filepath;
  let ifmatch = _req.query.ifmatch;

  searchFile(eID, fpath, _res);
};

async function searchFile(eID, filePath, _res) {

  let config = new ClientConfig();
  let credentials = new UserCredentials({
    'authorization': global.token
  });
  // Construct the FileServiceClient object
  let file_service_client = new FileServiceClient(config, credentials);

  try {
    const request_object = {
      entityId: eID,
      offset: "",
      limit: "",
      count: "",
      order: "",
      filter: "name eq '" + filePath + "'"
    };

    let file_data = await file_service_client.searchFiles(request_object);
    console.log(file_data);
    _res.write(file_data);

  } catch (ex) {
    // Exception handling
    console.log(ex);
    _res.end();
  }
  _res.end();
}
//-------------------------------------------------------------------------GET Files


exports.getFileWithID = function (_req, _res) {
  let eID = _req.query.eid;
  let fpath = _req.query.filepath;
  let ifmatch = _req.query.ifmatch;

  getFile(eID, fpath, ifmatch, _res);
};

exports.getFiles = function (_req, _res) {
  let eID = _req.query.eid;
  getAllFiles(eID, _res);
};

async function getAllFiles(eID, _res) {

  let config = new ClientConfig();
  let credentials = new UserCredentials({
    'authorization': global.token
  });
  // Construct the FileServiceClient object
  let file_service_client = new FileServiceClient(config, credentials);

  try {
    const request_object = {
      entityId: eID
    };

    let file_data = await file_service_client.searchFiles(request_object);
    _res.write(file_data);
  } catch (ex) {
    // Exception handling
    console.log(ex);
    _res.end();
  }
  _res.end();
}


async function getFile(eID, filePath, ifmatch, _res) {

  let config = new ClientConfig();
  let credentials = new UserCredentials({
    'authorization': global.token
  });
  // Construct the FileServiceClient object
  let file_service_client = new FileServiceClient(config, credentials);

  try {
    const request_object = {
      entityId: eID,
      filepath: filePath,
      //ifNoneMatch: ifmatch
    };

    let file_data = await file_service_client.getFile(request_object);
    let json_data = JSON.parse(file_data);
    _res.write(file_data);
  } catch (ex) {
    // Exception handling
    console.log(ex);
    _res.end();
  }
  _res.end();
}

//----------------------------------------------------Create Update

exports.createLogFile = function (eID, filePath, _req, _res) {
  let content = _req.body;
  let raport = validator.validateLogContent(content);
  if (raport.status) {
    filePath = filePath + content.entityId + content.aspectName;
    createUpdateLogFile(eID, filePath, content, _res);
  } else {
    _res.write(raport.message);
    _res.end();
  }
};


exports.createTresholdFile = function (eID, filePath, _req, _res) {
  let raport = validator.validateThresholdContent(_req.body);
  console.log(raport.status + " message: " + raport.message);
  if (raport.status) {
    let content = fm.validateThresholdFile(filePath, _req.body);
    createUpdateFile(eID, filePath, content, _res);
  } else {
    _res.send(raport.message);
    _res.end();//why take out? cos send also ends
  }
};

exports.deleteTresholdFile = function (eID, filePath, _req, _res) {
  let raport = validator.validateThresholdId(_req.body);
  console.log(raport.status + " message: " + raport.message);
  if (raport.status) {
    let content = fm.deleteThreshold(filePath, _req.body);
    if(content == undefined){
      _res.send("did not found threshold");
      _res.end();//why take out? cos send also ends
    }else{
      createUpdateFile(eID, filePath, content, _res);
    }
    
  } else {
    _res.send(raport.message);
    _res.end();//why take out? cos send also ends
  }
};


exports.createFile = function (eID, filePath, _req, _res) {
  createUpdateFile(eID, filePath, _req.body, _res)
};


async function createUpdateLogFile(eID, filePath, file, _res) {

  let config = new ClientConfig();
  let credentials = new UserCredentials({
    'authorization': global.token
  });
  let json_data = undefined;
  // Construct the FileServiceClient object
  let file_service_client = new FileServiceClient(config, credentials);
  let content = undefined;
  try {
    request_object = {
      entityId: eID,
      filepath: filePath,
      //ifNoneMatch: ifmatch
    };
    file_data = await file_service_client.getFile(request_object);
    json_data = JSON.parse(file_data);
  } catch (ex) {
    console.log("ERROR IN FILE CONTROL",ex);
  }
  try {
    content = fm.validateLogFile(filePath, json_data, file);
    createUpdateFile(eID, filePath, content, _res);
  } catch (err) {
    console.log("ERROR IN FILE CONTROL",err)
  }
}


async function createUpdateFile(eID, filePath, file, _res) {

  let config = new ClientConfig();
  let credentials = new UserCredentials({
    'authorization': global.token
  });
  // Construct the FileServiceClient object
  let file_service_client = new FileServiceClient(config, credentials);

  try {
    let request_object = {
      entityId: eID,
      offset: "",
      limit: "",
      count: "",
      order: "",
      filter: "name eq '" + filePath + "'"
    };

    let file_data = await file_service_client.searchFiles(request_object);
    let json_data = JSON.parse(file_data)

    if (!Object.keys(json_data).length) {
      request_object = {
        file: file,
        entityId: eID,
        filepath: filePath,
        type: "JSON"
      };
    } else {
      let ifmatch = json_data[0].etag;
      request_object = {
        file: file,
        entityId: eID,
        filepath: filePath,
        ifMatch: ifmatch,
        type: "JSON"
      };
    }
    await file_service_client.putFile(request_object);
    _res.send('success');
  } catch (ex) {
    // Exception handling
    console.log("ERROR IN FILE CONTROL",ex);
    _res.end();
  }
  _res.end();
}

//----------------------------------------------------Delete File

exports.deleteFileByName = function (_req, _res) {
  let eID = _req.query.eid;
  let fpath = _req.query.filepath;
  deleteFile(eID, fpath, _res);
};


async function deleteFile(eID, filePath, _res) {

  let config = new ClientConfig();
  let credentials = new UserCredentials({
    'authorization': global.token
  });
  // Construct the FileServiceClient object
  let file_service_client = new FileServiceClient(config, credentials);

  await file_service_client.deleteFile({
    entityId: eID,
    filepath: filePath
  });
  _res.write("true");
  _res.end();
}

//-------------------------TODO: make this better

exports.getStorageFile = async function (eID, filePath, ifmatch) {
  let config = new ClientConfig();
  let credentials = new UserCredentials({
    'authorization': global.token
  });
  // Construct the FileServiceClient object
  let file_service_client = new FileServiceClient(config, credentials);
  try {
    const request_object = {
      entityId: eID,
      filepath: filePath,
      ifNoneMatch: ifmatch
    };

    let file_data = await file_service_client.getFile(request_object);
    let json_data = JSON.parse(file_data);
    return json_data;
  } catch (ex) {
    // Exception handling
    console.log("ERROR IN FILE CONTROL",ex);
  }
  return undefined;
}