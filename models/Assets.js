/*
Assets model holds the data when switching to assets view in front end.
*/
class Assets {
    
    constructor(assetId,name,typeId) {
        this.assetId = assetId;
        this.name = name;
        this.typeId = typeId;
    }   
}

module.exports = Assets;