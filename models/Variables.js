/*
Variables model holds the data when switching to variables view in front end.
*/
class Variables {
    
    constructor(assetId,aspectName,name,value,unit,status) {
        this.assetId = assetId;
        this.aspectName = aspectName;
        this.name = name;
        this.value = value;
        this.unit = unit;
        this.status = status;
    }
}

module.exports = Variables;