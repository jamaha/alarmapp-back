/*
Thresholds model is used when handling thresholds

-id (string) is generated everytime when new threshold is created (created in validation.js)
-name (string) is the name of the threshold
-entityId (string) is assets ID
-aspectName (string) is aspects name
-variableName (string) is variables name

-start (string) is the clock time when threshold goes active (e.g. 08:00:00). 
When start is over 24 hour it is changed to 'ALL' value which tells that it is all days on

-hours (number) is the time how long threshold should be active

-active (string) is the date when the threshold should start to be active (e.g. 2019-12-08T00:00:00.000Z). 
Currently it has always value 'ALL' because we did not have the time to add it to front-end.

-expire (string) is the date when threshold should not be active (e.g. 2019-12-22T00:00:00.000Z). Currently empty.
-threshold (number) contains the value of the threshold
-sendTo (string) was supposed to be the name of the person/group who would receive the web socket message. This feature is not in use.
-type (string) has two states OVER or UNDER which defines will the threshold be activated when value is over or under the threshold
*/
class Thresholds {
    
    constructor(id,name,entityId,
        aspectName,
        variableName,
        start,
        hours,
        threshold,
        type) {
            this.id = id
            this.name = name,
            this.entityId = entityId,
            this.aspectName = aspectName,
            this.variableName = variableName,
            this.start = start,
            this.hours = hours,
            this.active = "ALL",
            this.expire = "",
            this.threshold = threshold,
            this.sendTo = "ALL",
            this.type = type
    }
}

module.exports = Thresholds;