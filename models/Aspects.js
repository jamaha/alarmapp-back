/*
Aspects model holds the data when switching to aspects view in front end.
*/
class Aspects {
    
    constructor(assetId,name,status) {
        this.assetId = assetId;
        this.name = name;
        this.status = status;
    }
}

module.exports = Aspects;