/*
Status contains all the different values what the activated job can have.
-name is status
-level is the priority how it shows which should be shown first in frontend.
If the same job has status UNSOLVED and FIXED it will use FIXED because the priority is higher 
*/

const STATUS = {
    
    UNSOLVED:{
        name: 'UNSOLVED',
        level: 1
    },
    PENDING: {
        name: 'PENDING',
        level: 2
    },
    FIXED: {
        name: 'FIXED',
        level: 3
    },
    NORMAL:{
        name: 'NORMAL',
        level: 0
    }
}

module.exports = STATUS;